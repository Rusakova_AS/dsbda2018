#!/bin/bash


start-all.sh
# Clean up folder (HDFS)
hdfs dfs -rm -R /metrics

# Generate data
python generator.py --num 1000 --id 4 --file metrics.txt
cp metrics.txt Producer/

# Build and run producer
cd Producer
mvn package
java -jar target/Producer-1-jar-with-dependencies.jar metrics.txt topic_1
cd ..

# Build and run application
cd AverageMetrics
mvn package > mvnfile
spark-submit --class com.hpproject.averagemetrics.Application target/AverageMetrics-1-jar-with-dependencies.jar hdfs://127.0.0.1:9000/metrics 1m topic_1 > runfile
cd ..

hadoop fs -cat /metrics/part-00000
