import time
from argparse import ArgumentParser
from random import randint


def get_arguments():
    parser = ArgumentParser(description="Generator")
    parser.add_argument("--file_name", type=str, default="metrics.txt", help='Name of input file.')
    parser.add_argument("--num", type=int, default=25000, help='Number of lines in the output.')
    parser.add_argument("--id", type=int, default=2, help='Number of different ids.')
    return parser.parse_args()

def main():

    argument = get_arguments()
    current_time = int(round(time.time() * 1000))

    with open(argument.file_name, 'w') as out:
        for i in range(0, argument.num):
            cur_id = randint(1, argument.id)
            out.write(
                str(cur_id) + "," +
                str(int(randint(1000000000000,current_time))) + "," +
                str( 30.0 + randint(-20, 20)) + "\n"
            )
            current_time += randint(0, 2)

if __name__ == "__main__":
    main()
