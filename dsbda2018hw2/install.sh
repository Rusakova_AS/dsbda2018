#1	Python
yum –y install https://centos7.iuscommunity.org/ius-release.rpm
yum install python36u
python3.6 –v
yum –y install python36u-pip
yum install python36u-devel 
#2	Spark
yum clean all
yum –y install epel-release
yum –y update
wget http://www.scala-lang.org/files/archive/scala-2.10.1.tgz
tar xvf scala-2.10.1.tgz
sudo mv scala-2.10.1 /usr/lib
sudo ln -s /usr/lib/scala-2.10.1 /usr/lib/scala
#Configure environment variables in .bash_profile.
echo 'export PATH=$PATH:/usr/lib/scala/bin' >> .bash_profile
echo 'export SPARK_HOME=$HOME/spark-2.4.0-bin-hadoop2.7' >> .bash_profile
echo 'export PATH=$PATH:$SPARK_HOME/bin' >> .bash_profile
scala –version
#Configue firwall.
firewall-cmd --permanent --zone=public --add-port=6066/tcp
firewall-cmd --permanent --zone=public --add-port=7077/tcp
firewall-cmd --permanent --zone=public --add-port=8080-8081/tcp
firewall-cmd --reload
