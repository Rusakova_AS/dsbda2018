/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.averagemetrics;

import com.holdenkarau.spark.testing.JavaRDDComparisons;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.*;
import scala.Tuple2;
import scala.Tuple3;

import com.holdenkarau.spark.testing.SharedJavaSparkContext;
import org.junit.Before;
import org.junit.Test;
import scala.reflect.ClassTag;

/**
 *
 * @author master
 */
public class RDDTest extends SharedJavaSparkContext implements Serializable {

    private static JavaRDD<String> dataset;
    private static List<String> metrics;
    private static List<Tuple2<Integer, Long>> validation_value1;
    private static List<Tuple2<Integer, Double>> validation_value2;
    private static List<Tuple2<Integer, Double>> validation_value3;
    private static List<Tuple2<Integer, Long>> validation_value4;
    private static List<Tuple2<Integer, Tuple3<Long, String, Double>>> validation_value5;

    @Before
    public void setUp() throws Exception {
        metrics = Arrays.asList("2,1397463641,28.0",
                "1,1007601605,16.0",
                "2,1186879717,37.0");
        validation_value1 = Arrays.asList(new Tuple2<>(2, Long.valueOf(1397463641)),
                new Tuple2<>(1, Long.valueOf(1007601605)), new Tuple2<>(2, Long.valueOf(1186879717)));
        validation_value2 = Arrays.asList(new Tuple2<>(2, 28.0), new Tuple2<>(1, 16.0), new Tuple2<>(2, 37.0));
        validation_value3 = Arrays.asList(new Tuple2<>(1, 16.0), new Tuple2<>(2, 32.5));
        validation_value4 = Arrays.asList(new Tuple2<>(1, Long.valueOf(1007601605)), new Tuple2<>(2, Long.valueOf(1186879717)));
        validation_value5 = Arrays.asList(new Tuple2<>(1,new Tuple3<>(Long.valueOf(1007601605),"1m", 16.0)),
                new Tuple2<>(2,new Tuple3<>(Long.valueOf(1186879717),"1m", 32.5)));
    }

    @Test
    public void verifySplitStampTest() {
        JavaRDD<String> rdd1 = jsc().parallelize(metrics);
        JavaPairRDD<Integer, Long> valid_value = jsc().parallelizePairs(validation_value1);

        JavaPairRDD<Integer, Long> result = rdd1.mapToPair(s -> {
            String[] parts = s.split(",");
            return new Tuple2<>(Integer.parseInt(parts[0]), Long.parseLong(parts[1]));
        });

        ClassTag<Tuple2<Integer, Long>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        /*
            Run the assertions on the result and expected
        */
        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

    @Test
    public void verifySplitMetricsTest() {
        JavaRDD<String> rdd1 = jsc().parallelize(metrics);
        JavaPairRDD<Integer, Double> valid_value = jsc().parallelizePairs(validation_value2);

        JavaPairRDD<Integer, Double> result = rdd1.mapToPair(s -> {
            String[] parts = s.split(",");
            return new Tuple2<>(Integer.parseInt(parts[0]), Double.parseDouble(parts[2]));
        });

        ClassTag<Tuple2<Integer, Double>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        /*
            Run the assertions on the result and expected
        */
        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

    @Test
    public void verifyAvgCountTest() {
        JavaPairRDD<Integer, Double> rdd1 = jsc().parallelizePairs(validation_value2);
        JavaPairRDD<Integer, Double> valid_value = jsc().parallelizePairs(validation_value3);

        /*
            Compute average value of metrics.
         */
        Function<Double, AvgCount> createComb = (Double y) -> new AvgCount(y, 1);
        Function2<AvgCount, Double, AvgCount> addAndCount = (AvgCount x, Double y) -> {
            x.addTotal(y);
            x.addCount(1);
            return x;
        };
        Function2<AvgCount, AvgCount, AvgCount> combine = (AvgCount x, AvgCount y) -> {
            x.addTotal(y.getTotal());
            x.addCount(y.getCount());
            return x;
        };

        JavaPairRDD<Integer, Double> result = rdd1.combineByKey(createComb, addAndCount, combine)
                .mapToPair((PairFunction<Tuple2<Integer, AvgCount>, Integer, Double>) f -> new Tuple2<>(f._1, f._2.avg()));

        ClassTag<Tuple2<Integer, Double>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        /*
            Run the assertions on the result and expected
        */
        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

    @Test
    public void verifyMinValueTest() {
        JavaPairRDD<Integer, Long> rdd1 = jsc().parallelizePairs(validation_value1);
        JavaPairRDD<Integer, Long> valid_value = jsc().parallelizePairs(validation_value4);

        /*
            Compute minimum time stamp for every id.
        */
        JavaPairRDD<Integer, Long> result = rdd1.reduceByKey((v1, v2) -> Math.min(v1, v2));

        ClassTag<Tuple2<Integer, Long>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        /*
            Run the assertions on the result and expected
        */
        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }
    
    @Test
    public void verifyResultTest() {
        JavaPairRDD<Integer, Long> rdd1 = jsc().parallelizePairs(validation_value4);
        JavaPairRDD<Integer, Double> rdd2 = jsc().parallelizePairs(validation_value3);
        JavaPairRDD<Integer, Tuple3<Long, String, Double>> valid_value = jsc().parallelizePairs(validation_value5);

        /*
            Form result by joining RDD's and adding scale column.
        */
        JavaPairRDD<Integer, Tuple3<Long, String, Double>> result = rdd1.join(rdd2)
                .mapValues((Function<Tuple2<Long, Double>, Tuple3<Long, String, Double>>) f -> new Tuple3<>(f._1,"1m",f._2));

        ClassTag<Tuple2<Integer, Tuple3<Long, String, Double>>> tag = scala.reflect.ClassTag$.MODULE$.apply(Tuple2.class);

        /*
            Run the assertions on the result and expected
        */
        JavaRDDComparisons.assertRDDEquals(
                JavaRDD.fromRDD(JavaPairRDD.toRDD(result), tag),
                JavaRDD.fromRDD(JavaPairRDD.toRDD(valid_value), tag));
    }

}
