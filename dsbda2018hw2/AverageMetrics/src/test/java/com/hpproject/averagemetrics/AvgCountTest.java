/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.averagemetrics;

import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.*;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author master
 */
public class AvgCountTest {

    private static Double total;
    private static Integer count;


    @Before
    public void setUp() throws Exception {
        total = 17.5;
        count = 10;
    }

    /*
         Test custom AvgCount class
    */
    @Test
    public void AvgCountClassTest() {
        AvgCount test = new AvgCount(total, count);
        assertNotNull(test);
        assertEquals(17.5, test.getTotal(), 0.000001);
        assertEquals(Integer.valueOf(10), test.getCount());
        test.addTotal(2.0);
        assertEquals(19.5, test.getTotal(), 0.000001);
        test.addCount(1);
        assertEquals(Integer.valueOf(11), test.getCount());
        assertEquals(19.5 / 11, test.avg(), 0.000001);
    }

}
