/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.averagemetrics;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author master
 */
public class ProdConsTest {

    private static String brokers;
    private static String username;
    private static String passwd;
    private static Properties props;
    private static String topic;
    private static List<String> line;
    private static String jaasTemplate;
    private static String jaasCfg;
    private static String serializer;
    private static String deserializer;
    private static Producer<String, String> producer;
    private static Iterator<String> iter1;
    private static Iterator<String> iter2;
    private static KafkaConsumer<String, String> consumer;

    @Before
    public void setUp() throws Exception {
        brokers = "velomobile-01.srvs.cloudkafka.com:9094,velomobile-02.srvs.cloudkafka.com:9094,velomobile-03.srvs.cloudkafka.com:9094";
        username = "37xh4e1y";
        passwd = "cNDBgtUEbTr03raSOtFdGtA39FAthF7l";
        topic = username + "-test";
        jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
        jaasCfg = String.format(jaasTemplate, username, passwd);
        line =  Arrays.asList("2,1397463641577,28.0","1,1397555551577,12.0", "2,1397463641111,10.0");
        iter1 = line.iterator();
        iter2 = line.iterator();
  }

    @Test
    public void testProducerAndConsumer() {

        serializer = StringSerializer.class.getName();
        deserializer = StringDeserializer.class.getName();
        props = new Properties();
        props.put("bootstrap.servers", brokers);
        props.put("group.id", username + "-testconsumer");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("auto.offset.reset", "earliest");
        props.put("session.timeout.ms", "6000");
        props.put("key.deserializer", deserializer);
        props.put("value.deserializer", deserializer);
        props.put("key.serializer", serializer);
        props.put("value.serializer", serializer);
        props.put("security.protocol", "SASL_SSL");
        props.put("sasl.mechanism", "SCRAM-SHA-256");
        props.put("sasl.jaas.config", jaasCfg);

        /*
            Define the producer  
        */
        

        producer = new KafkaProducer<>(props);
 
        Thread one = new Thread() {
            public void run() {
                try {
                    producer = new KafkaProducer<>(props);

                    while (iter1.hasNext()) {
                        String record = iter1.next();

                        producer.send(new ProducerRecord<>(topic, 0, null, record));
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException v) {
                    System.out.println(v);
                }
            }
        };
        one.start();

        /*
            Define the consumer
        */
        
        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(topic));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(1000);
            if (!records.isEmpty()) {
                for (ConsumerRecord<String, String> record : records) {
                    assertEquals(null, record.key());
                    assertEquals(iter2.next(), record.value());
                    assertEquals(topic, record.topic());
                }
            } else {
                break;
            }
        }

    }

}
