/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.averagemetrics;
import java.io.*;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.*;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;
import scala.Tuple3;
import java.util.ArrayList;
/**
 *
 * @author master
 */
public class Application {

    /*
        Brokers, username, password was gotten from CloudKarafka.
    */
    private static String brokers = "velomobile-01.srvs.cloudkafka.com:9094,velomobile-02.srvs.cloudkafka.com:9094,velomobile-03.srvs.cloudkafka.com:9094";
    private static String username = "37xh4e1y";
    private static String passwd = "cNDBgtUEbTr03raSOtFdGtA39FAthF7l";

    public static void main(String[] args) throws Exception {

        /*
            First arg is input file name, second arg is the value of scale, third arg is topic
         */
        if (args.length < 3) {
            throw new Exception("Less than 2 arguments. Output, scale, topic");
        }

        ConsumerClass kafka_session = new ConsumerClass(brokers, username, passwd, args[2]);

        /*
            Create SparkSession
        */
        SparkSession session = SparkSession
                .builder()
                .master("local")
                .appName("Hw2 Kafka Spark RDD HDFS")
                .getOrCreate();
        /*
            Create short log
        */
        File myfile = new File("statistic"); 
        BufferedWriter writer = new BufferedWriter(new FileWriter(myfile, true));
        String linsep = System.getProperty("line.separator");	
        writer.write("Amount of items on each step:\n");
 
        /*
            Create java Spark context
        */
        JavaSparkContext javaSparkContext = new JavaSparkContext(session.sparkContext());
        ArrayList <String> consume_result = kafka_session.consume();
        writer.write("Kafka lines " + consume_result.size() + "\n");
 
        JavaRDD<String> dataset = javaSparkContext.parallelize(consume_result);

        /*
            Get RDD's for computing average metric and minimum stamp for every id.
        */
        JavaPairRDD<Integer, Long> stamps = dataset.mapToPair(s -> {
            String[] parts = s.split(",");
            return new Tuple2<>(Integer.parseInt(parts[0]), Long.parseLong(parts[1]));
        });
        writer.write("Stamps " + stamps.count() + "\n");
       
        JavaPairRDD<Integer, Double> metrics = dataset.mapToPair(s -> {
            String[] parts = s.split(",");
            return new Tuple2<>(Integer.parseInt(parts[0]), Double.parseDouble(parts[2]));
        });
        writer.write("Metrics " + metrics.count() + "\n");
 
        /*
            Compute average value of metrics.
        */
        Function<Double, AvgCount> createComb = (Double y) -> new AvgCount(y, 1);
        Function2<AvgCount, Double, AvgCount> addAndCount = (AvgCount x, Double y) -> {
            x.addTotal(y);
            x.addCount(1);
            return x;
        };
        Function2<AvgCount, AvgCount, AvgCount> combine = (AvgCount x, AvgCount y) -> {
            x.addTotal(y.getTotal());
            x.addCount(y.getCount());
            return x;
        };

        JavaPairRDD<Integer, Double> avgCounts = metrics.combineByKey(createComb, addAndCount, combine)
                .mapToPair((PairFunction<Tuple2<Integer, AvgCount>, Integer, Double>) f -> new Tuple2<>(f._1, f._2.avg()));
        writer.write("Avg " + avgCounts.count() + "\n");
 
        /*
            Compute minimum time stamp for every id.
        */
        JavaPairRDD<Integer, Long> min = stamps.reduceByKey((v1, v2) -> Math.min(v1, v2));
        
      	writer.write("Min " + min.count() + "\n");
 
        /*
            Form result by joining RDD's and adding scale column.
        */
        JavaPairRDD<Integer, Tuple3<Long, String, Double>> joined = min.join(avgCounts)
                .mapValues((Function<Tuple2<Long, Double>, Tuple3<Long, String, Double>>) f -> new Tuple3<>(f._1, args[1], f._2)).sortByKey();
       
        writer.write("Joined " + joined.count() + "\n");

        /*
            Save to hdfs.
        */

        joined.saveAsTextFile(args[0]);
        writer.close();
        session.close();
    }
}
