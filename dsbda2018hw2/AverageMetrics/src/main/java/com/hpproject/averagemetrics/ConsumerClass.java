/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.averagemetrics;

import java.util.ArrayList;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.util.Arrays;
import java.util.Properties;

/**
 *
 * @author master
 */
public class ConsumerClass {

    private final String topic;
    private final Properties props;

    public ConsumerClass(String brokers, String username, String password, String t) {
        /*
            Configure connection to Kafka broker: broker, username, password, topic
        */
        this.topic = username + "-" + t;

        String jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
        String jaasCfg = String.format(jaasTemplate, username, password);

        String deserializer = StringDeserializer.class.getName();
        props = new Properties();
        props.put("bootstrap.servers", brokers);
        props.put("group.id", username + "-consumer-" + System.currentTimeMillis());
        props.put("session.timeout.ms",6000);
        props.put("auto.offset.reset", "earliest");
        props.put("key.deserializer", deserializer);
        props.put("value.deserializer", deserializer);
        props.put("security.protocol", "SASL_SSL");
        props.put("sasl.mechanism", "SCRAM-SHA-256");
        props.put("sasl.jaas.config", jaasCfg);
    }

    /*
        Consumer reads messages from topic and add lines to List of Strings. The loop ends if no entries appear for 3 seconds
    */
    public ArrayList<String> consume() {

        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        ArrayList<String> metrics = new ArrayList<>();
        consumer.subscribe(Arrays.asList(topic));
        int count_fails = 0;
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(1000);
            if (!records.isEmpty()) {
                for (ConsumerRecord<String, String> record : records) {
                    metrics.add(record.value());
                    /*System.out.printf("%s [%d] offset=%d, key=%s, value=\"%s\"\n",
                                                                  record.topic(), record.partition(),
                                                                  record.offset(), record.key(), record.value());*/
                }
            } else {
                count_fails++;
                if (count_fails == 3)
                    break;
            }
        }
        return metrics;
    }
}
