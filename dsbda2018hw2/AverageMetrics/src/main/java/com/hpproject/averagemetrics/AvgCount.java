/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.averagemetrics;

import java.io.Serializable;

/**
 *
 * @author master
 */

 /*
    Average value computing
 */
public class AvgCount implements Serializable {

    private Double total;
    private Integer count;
    
    public AvgCount(Double total_value, Integer count_value) {
        total = total_value;
        count = count_value;
    }

    public Double getTotal(){
        return total;
    }
 
    public Integer getCount(){
        return count;
    }
    public Double avg() {
        return total / count;
    }
    
    public void addTotal(Double v) {
        total += v;
    }
    public void addCount(Integer v) {
        count += v;
    }
}
