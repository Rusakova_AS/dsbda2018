/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.producer;

/**
 *
 * @author Master
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author Master
 */
public class ProducerClass {

    private final String topic;
    private final Properties props;
       
    public ProducerClass(String brokers, String username, String password, String t) {
      
        /*
            Configure connection to Kafka broker: broker, group id, username, password, topic
         */
        this.topic = username + "-" + t;

        String jaasTemplate = "org.apache.kafka.common.security.scram.ScramLoginModule required username=\"%s\" password=\"%s\";";
        String jaasCfg = String.format(jaasTemplate, username, password);

        String serializer = StringSerializer.class.getName();
        String deserializer = StringDeserializer.class.getName();
        props = new Properties();
        props.put("bootstrap.servers", brokers);
        props.put("key.serializer", serializer);
        props.put("value.serializer", serializer);
        props.put("security.protocol", "SASL_SSL");
        props.put("sasl.mechanism", "SCRAM-SHA-256");
        props.put("sasl.jaas.config", jaasCfg);
    }

    /*
        Producer publish one sentence every second.
    */
    public void produce(String path) throws IOException {
        List<String> metrics = Files.readAllLines(Paths.get(path));

        Iterator<String> iter = metrics.iterator();
        Thread one = new Thread() {
            public void run() {
                try {
                    Producer<String, String> producer = new KafkaProducer<>(props);

                    while (iter.hasNext()) {
                        String record = iter.next();

                        /*
                                Producer sends the sentence to the topic every second.
                         */
                        producer.send(new ProducerRecord<>(topic, 0, null, record));
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException v) {
                    System.out.println(v);
                }
            }
        };
        one.start();
    }

    public static void main(String[] args) throws Exception {
        
        /*
            First arg is input file name. Second is topic.
        */
        String username = "37xh4e1y";
        String brokers = "velomobile-01.srvs.cloudkafka.com:9094,velomobile-02.srvs.cloudkafka.com:9094,velomobile-03.srvs.cloudkafka.com:9094";
        String passwd = "cNDBgtUEbTr03raSOtFdGtA39FAthF7l";
 
        if (args.length < 2) {
            throw new Exception("Less than 2 arguments. Input file, topic");
        }

        ProducerClass c = new ProducerClass(brokers, username, passwd, args[1]);
        c.produce(args[0]);

    }
}
