#!/bin/bash
hdfs dfs -rm -R /input /cache /output
rm -fR /opt/hadoop/result/*

wget -O DataSet.zip "http://goo.gl/lwgoxw"
unzip DataSet.zip \*imp.201310\*.txt.bz2 \*city.en.txt

hdfs dfs -mkdir /input
hdfs dfs -mkdir /cache

hdfs dfs -put /opt/hadoop/ipinyou.contest.dataset/city.en.txt /cache

for((i=19; i<28; i++))
do
bzip2 -d /opt/hadoop/ipinyou.contest.dataset/training3rd/imp.201310$i.txt.bz2
hdfs dfs -put /opt/hadoop/ipinyou.contest.dataset/training3rd/imp.201310$i.txt /input/imp.201310$i.txt
done

cd /opt/hadoop/project/Bid/bidprice
mvn dependency:resolve
mvn test
hadoop jar ./target/bidprice-1-jar-with-dependencies.jar CountofItems

hdfs dfs -get /output/part-r-00* /opt/hadoop/result 
exit 0
