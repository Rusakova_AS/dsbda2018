/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hpproject.bidprice;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;

public class WCclass implements WritableComparable<WCclass>{//custom  WritableComparable class

    private Text city;
    private Text osname;
    
    public WCclass(){
        city = new Text();
        osname = new Text();
    }
    
    public WCclass(Text city, Text osname) {
        this.city = city;
        this.osname = osname;
    }
    
    public Text getCity() {
        return city;
    }
    
    public Text getOsname() {
        return osname;
    }
    public void set(Text first, Text second) {
        city = first;
        osname = second;
    }
    @Override
    public int compareTo(WCclass o) {
        if (city.compareTo(o.city) != 0) {
            return city.compareTo(o.city);
        }
        return osname.compareTo(o.osname);       
    }
	
    @Override
    public String toString() {
    	return city.toString() + " " + osname.toString();    
    }

    @Override
    public void write(DataOutput d) throws IOException {
        city.write(d);
        osname.write(d);
    }

    @Override
    public void readFields(DataInput di) throws IOException {
        city.readFields(di);
        osname.readFields(di);
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + city.hashCode();
        result = prime * result + osname.hashCode();
        return result;
    }
    @Override
    public boolean equals(Object o)
    {
        if(o instanceof WCclass)
        {
            WCclass tp = (WCclass) o;
            return city.equals(tp.city) && osname.equals(tp.osname);
        }
        return false;
    }
}
